from tqdm import tqdm
from bs4 import BeautifulSoup as bs
import requests
import os
import sys

urlGen = "https://picstatio.com"
urlList = sys.argv[1:]

def dlFile(k):
    dlUrl = urlGen + k + "/download"
    fileNameLong = dlUrl.split("/")[-2].split("-")
    fileID = fileNameLong[-1]
    fileName = "-".join(fileNameLong[:-1])
    finUrl = urlGen + "/u/" + fileID + "/" + fileName + ".jpg"
    print(finUrl)
    
    response = requests.get(finUrl, stream=True)

    with open(fileName + ".jpg", "wb") as handle:
        for data in tqdm(response.iter_content()):
            handle.write(data)

def dlUrlGroup(urlSpec):
    if not os.path.isdir(urlSpec):
        os.makedirs(urlSpec)
    
    os.chdir(urlSpec)
    
    lenUrlSpec = len(urlSpec)
    
    url = urlGen + "/" + urlSpec
    headers = {'User-Agent': 'Mozilla/5.0'}
    page = requests.get(url)
    
    soup = bs(page.text, "html.parser")
    
    links = soup.findAll("a")
    links = list(set(links))

    minPage = 2
    maxPage = 0
    
    print()
    print("Page 1")
    print()
    
    for i in links:
        k = i["href"]
        if "/wallpaper/" in k and k not in ["/wallpaper/popular-monthly", "/wallpaper/popular-all"]:
            dlFile(k)
        elif "/page/" in k:
            j = int(k[lenUrlSpec+7:])
            if j > maxPage:
                maxPage = j
    
    for l in range(2, maxPage+1):
        print()
        print("Page", l)
        print()
    
        urlPage = url + "/page/" + str(l)
    
        newPage = requests.get(urlPage)
        newSoup = bs(newPage.text, "html.parser")
    
        links = newSoup.findAll("a")
        links = list(set(links))
    
        for i in links:
            k = i["href"]
            if "/wallpaper/" in k and k not in ["/wallpaper/popular-monthly", "/wallpaper/popular-all"]:
                dlFile(k)

for urlSpec in urlList:   
    dlUrlGroup(urlSpec)
    os.chdir("..")
